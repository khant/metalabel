import { Nav, Navbar } from "react-bootstrap";
import { ColorRing } from "react-loader-spinner";
import styles from "../styles/SimpleNav.module.css";
import { useState } from "react";
import { useRouter } from "next/router";
import clsx from "clsx";

function NavSimple() {
  const [active, setActive] = useState();
  const router = useRouter();
  return (
    <>
      <Navbar fixed="top" bg="dark" variant="dark">
        <Navbar.Brand className={styles.brand} href="/">
          <b>Metalabel 📚</b>
        </Navbar.Brand>
        <Nav className="me-auto" activeKey={router.pathname}>
          <Nav.Link className={styles.link} eventKey="/add" href="/add">
            Add Metalabels
          </Nav.Link>
          <Nav.Link className={styles.link} eventKey="/label" href="/label">
            Label Data
          </Nav.Link>
          <Nav.Link className={styles.link} eventKey="/results" href="/results">
            Results
          </Nav.Link>
          <Nav.Link className={styles.link} eventKey="/docs" href="/docs">
            Documentation
          </Nav.Link>   
          <Nav.Link disabled className={clsx(styles.project, "ms-auto")} >
            <b>Project name: </b>{process.env.NEXT_PUBLIC_METALABEL_PROJECT_NAME}
          </Nav.Link>
        </Nav>
      </Navbar>
      <br />
      <br />
    </>
  );
}

export { NavSimple };
