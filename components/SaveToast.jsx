import Toast from "react-bootstrap/Toast";
import ToastContainer from "react-bootstrap/ToastContainer";
function SaveToast({show, setShow}) {
  return (
    <ToastContainer className="p-3" position="bottom-end">
      <Toast onClose={() => setShow(false)} show={show} delay={3000} autohide>
        <Toast.Header className="primary">
          <img src="holder.js/20x20?text=%20" className="rounded me-2" alt="" />
          <strong className="me-auto">Saved to JSON ✅</strong>
        </Toast.Header>
      </Toast>
    </ToastContainer>
  );
}

export { SaveToast };
