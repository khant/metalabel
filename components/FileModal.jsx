import { clsx } from "clsx";
import { Button, Modal, Dropdown } from "react-bootstrap";
import { useState, useEffect } from "react";
import axios from "axios";
import * as dfd from "danfojs";
import { DataFrame, readCSV, readJSON } from "danfojs";
import { Tabs, Tab } from "react-bootstrap";
import { MetaLabelInput } from "../components/MetaLabelInput";

function FileModal({
  handleClose,
  fileData,
  fURL,
  handleMetaLabels,
  show,
  ftype,
  modalClosed,
}) {
  const [headers, setHeaders] = useState();
  const [currentSheet, setCurrentSheet] = useState("");
  const [loaded, setLoaded] = useState(false);
  const [metaLabels, setMetaLabels] = useState([]);

  const handleMetalabelInput = (data) => {
    setMetaLabels(data);
    handleMetaLabels(data);
  };
  const handleSheetSelect = (e) => {
    setCurrentSheet(e);
  };
  useEffect(() => {
    if (ftype === "csv") {
      console.log(fileData);
      readCSV(fileData)
        .then((df) => {
          //Get headers in the a state variable
          setHeaders(df.columns);
          setLoaded(true);
          //Plotly table header styling
          const headerStyle = {
            align: "center",
            fill: { color: ["gray"] },
            font: { family: "Arial", size: 12, color: "white" },
          };
          //Plotly cell styling
          const cellStyle = {
            align: ["center", "center"],
            line: { color: "black", width: 1 },
          };
          df.plot("plot_div").table({
            config: {
              tableHeaderStyle: headerStyle,
              tableCellStyle: cellStyle,
              displaylogo: false,
            },
          });
        })
        .catch((err) => {
          console.log(err);
        });
    }
    if (ftype === "xlsx" && currentSheet != undefined) {
      const selected = fileData?.find(
        (element) => element.name === currentSheet
      );
      const df = new DataFrame(selected?.data);
      setHeaders(df.columns);
      setLoaded(true);
      const headerStyle = {
        align: "center",
        fill: { color: ["gray"] },
        font: { family: "Arial", size: 12, color: "white" },
      };
      //Plotly cell styling
      const cellStyle = {
        align: ["center", "center"],
        line: { color: "black", width: 1 },
      };
      df.plot("plot_div").table({
        config: {
          tableHeaderStyle: headerStyle,
          tableCellStyle: cellStyle,
          displaylogo: false,
        },
      });
    }
  }, [fileData, currentSheet]);

  useEffect(() => {
    if (modalClosed === true) {
      setCurrentSheet("");
    }
  }, [modalClosed]);

  if (ftype === "xlsx" ) {
    var ListSheetNames = fileData?.map((obj, index) => {
      return (
        <Dropdown.Item key={index} eventKey={obj.name}>
          {obj.name}
        </Dropdown.Item>
      );
    });
  }
  return (
    <Modal show={show} onHide={handleClose} dialogClassName="modal-90w">
      <Modal.Header closeButton>
        <Modal.Title>Add Metalabels</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Tabs
          defaultActiveKey="profile"
          id="uncontrolled-tab-example"
          className="mb-3"
        >
          <Tab eventKey="metalabels" title="Metalabels">
            {loaded ? (
              <MetaLabelInput
                headers={headers}
                fURL={fURL}
                handleMetalabelInput={handleMetalabelInput}
              />
            ) : (
              <div>...Loading</div>
            )}
          </Tab>
          <Tab eventKey="profile" title="File Data">
            <div>
              {ftype === "xlsx" ? (
                <Dropdown onSelect={handleSheetSelect}>
                  <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                    {currentSheet === "" ? "Select Sheet" : currentSheet}
                  </Dropdown.Toggle>
                  <Dropdown.Menu>{ListSheetNames}</Dropdown.Menu>
                </Dropdown>
              ) : (
                <></>
              )}
            </div>

            <div id="plot_div"></div>
          </Tab>
        </Tabs>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
        <Button variant="primary" onClick={handleClose}>
          Save Changes
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export { FileModal };
