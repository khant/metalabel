import Card from "react-bootstrap/Card";
import { clsx } from "clsx";
import Button from "react-bootstrap/Button";
import { useState } from "react";
import axios from "axios";
import Badge from "react-bootstrap/Badge";
import { FileModal } from "./FileModal";
import { ColorRing } from "react-loader-spinner";
import { useEffect } from "react";
import styles from "../styles/FileCard.module.css";
import { useSemiPersistantState } from "../hooks/SemiPersistantState";
import { useResult } from "../contexts/ResultContext";

//import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

function FileCard({ files }) {
  var datafile;
  const { results, setResults } = useResult();
  const [show, setShow] = useState(false);
  var [fileData, setFileData] = useState();
  const [fURL, setFURL] = useState();
  const [ftype, setFtype] = useState();
  const [metalabel, setMetaLabel] = useState();
  const [modalClosed, setModalClosed] = useState(true);

  const handleClose = () => {
    axios
      .post("/api/results_write", { metalabel: metalabel })
      .then((res) => {
        console.log(res);
        setResults(res.data);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {});
    setShow(false);
    setModalClosed(true);
  };

  const handleMetalabels = (data) => {
    setMetaLabel(data);
  };

  const listItems = files.map((obj, index) => {
    const [labeled, setLabeled] = useState(false);
    useEffect(() => {
      if (
        results != undefined &&
        results.some((e) => e.fpath === files[index].path)
      )
      //TODO: Add "not all labeled" badge for only when some of the columns are labeled
        setLabeled(true);
    }, [results]);
    const handleShow = () => {
      setModalClosed(false);
      setFURL(files[index].path);
      setFtype(files[index].type);
      setShow(true);
      axios
        .post("/api/file-reader/file", {
          file: files[index].path,
          ftype: files[index].type,
        })
        .then((res) => {
          if (files[index].type === "csv") {
            datafile = new Blob([res.data], { type: "text/csv" });
            setFileData(new Blob([res.data], { type: "text/csv" }));
          }
          if (files[index].type === "xlsx") setFileData(res.data);
        });
    };
    return (
      <div key={index}>
        <Card>
          <Card.Header variant="secondary">
            <b>{obj.type}</b>
            <p className={styles.size}>
              <b>File size: </b>
              {obj.stats.size / 1000000} MBs
            </p>
          </Card.Header>
          <Card.Body>
            <Card.Title>
              <b>
                <u>File Path:</u>
              </b>
              {obj.path}
            </Card.Title>
            <hr></hr>
            <Button variant="primary" onClick={handleShow}>
              Open
            </Button>
            <Badge
              className={clsx(styles.badge)}
              bg={labeled ? "success" : "danger"}
              pill
            >
              {labeled ? <>Labeled</> : <>Not Labeled</>}
            </Badge>
          </Card.Body>
        </Card>
        <br />
        <br />
      </div>
    );
  });

  return (
    <>
      <div>{listItems}</div>

      <FileModal
        show={show}
        handleClose={handleClose}
        fileData={fileData}
        fURL={fURL}
        ftype={ftype}
        handleMetaLabels={handleMetalabels}
        modalClosed={modalClosed}
      />
    </>
  );
}

export { FileCard };
