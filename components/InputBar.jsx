import { clsx } from "clsx";
import { useState, useRef } from "react";
import axios from "axios";
import {
  InputGroup,
  Button,
  Row,
  Col,
  Container,
  Nav,
  Navbar,
  Form,
  OverlayTrigger,
  Tooltip,
  DropdownButton,
  Dropdown,
} from "react-bootstrap";
import { ColorRing } from "react-loader-spinner";
import styles from "../styles/InputBar.module.css";
import { useEffect } from "react";
import { BsFillPlusSquareFill, BsSearch } from "react-icons/bs";
import { useRouter } from 'next/router';


function InputBar({ dataCallback }) {
  const router = useRouter();
  const [value, setValue] = useState();
  const [hideButton, setHideButton] = useState(true);
  const [loading, setLoading] = useState(false);
  const [csv, setCSV] = useState(false);
  const [xlsx, setXLSX] = useState(false);
  const [show, setShow] = useState(false);
  const target = useRef(null);
  const onInput = ({ target: { value } }) => setValue(value);
  const onCSV = () => {
    if (csv === false) setCSV(true);
    if (csv === true) setCSV(false);
  };
  const onXLSX = () => {
    if (xlsx === false) setXLSX(true);
    if (xlsx === true) setXLSX(false);
  };

  const renderTooltip = (props) => (
    <Tooltip id="button-tooltip" {...props}>
      Add metalabelgroups
    </Tooltip>
  );

  const onFormSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    axios
      .post("/api/fswalk", { url: value, csv: csv, xlsx: xlsx })
      .then((res) => {
        dataCallback(res.data);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    if (csv === true || xlsx === true) {
      setHideButton(false);
    } else {
      setHideButton(true);
    }
  }, [csv, xlsx]);

  return (
    <>
      <Navbar fixed="top" bg="dark" variant="dark" className="justify-content-end">
        <Navbar.Brand className={styles.brand} href="/">
          <b>Metalabel 📚</b>
        </Navbar.Brand>
        <Nav activeKey={router.pathname} className="me-auto my-2 my-lg-0">
          <Nav.Item>
            <Form
              className={clsx(styles.form, "d-flex")}
              onSubmit={onFormSubmit}
            >
         
          
                  <Form.Group
                    className={clsx("mb-3", styles.input)}
                    controlId="url"
                  >
                    <InputGroup className="mb-3">
                    <Dropdown >
                        <DropdownButton
                          autoClose={false}
                          className={styles.dropdownButton}
                          variant="primary"
                          title="Select File Types"
                          id="input-group-dropdown-1"
                        >
                          <Dropdown.Item as="button">
                            <Form.Check
                              type="checkbox"
                              id="disabledFieldsetCheck"
                              label=".csv"
                              onChange={onCSV}
                            />
                          </Dropdown.Item>
                          <Dropdown.Divider />
                          <Dropdown.Item as="button">
                            <Form.Check
                              type="checkbox"
                              id="disabledFieldsetCheck"
                              label=".xlsx/xls"
                              onChange={onXLSX}
                            />
                          </Dropdown.Item>
                        </DropdownButton>
                      </Dropdown>

                      <Form.Control
                        required
                        onChange={onInput}
                        value={value}
                        natype="path"
                        placeholder="..path/..to/..folder"
                      />
                      <Form.Control.Feedback type="invalid">
                        Please add a URL to a local folder.
                      </Form.Control.Feedback>
                  

                      <Button
                        variant="primary"
                        type="submit"
                        disabled={hideButton}
                      >
                        <BsSearch></BsSearch>
                        {loading && (
                          <ColorRing
                            visible={true}
                            height="20"
                            width="20"
                            ariaLabel="blocks-loading"
                            wrapperClass="blocks-wrapper"
                            colors={[
                              "#0070f3",
                              "#74A22B",
                              "#0070f3",
                              "#74A22B",
                            ]}
                          />
                        )}
                      </Button>
                    </InputGroup>
                  </Form.Group>
          
            
            </Form>
          </Nav.Item>
        <Nav.Item className={styles.link}> 
         <Nav.Link href="/add">Add Metalabels</Nav.Link>
         
        </Nav.Item>
        <Nav.Item className={styles.link}>
         <Nav.Link href="/results">Results</Nav.Link>
         
        </Nav.Item>
        <Nav.Item className={styles.link}>
         <Nav.Link href="/docs">Documentation</Nav.Link>
         
        </Nav.Item>
  
   {/*      <Nav.Item className={styles.link}> 
         <Nav.Link href="/label">Label</Nav.Link>
         
        </Nav.Item> */}
        
        </Nav>
       
      </Navbar>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
    </>
  );
}

export { InputBar };
