import Form from "react-bootstrap/Form";
import { useState, useEffect } from "react";
import { useMetalabels } from "../contexts/MetalabelContext";
import { useResult } from "../contexts/ResultContext";

function MetaLabeSelect({ fPath, currentHeader, handleSelections }) {
  const { metalabels, setMetalabels } = useMetalabels();
  const { results, setResults } = useResult();
  const [current, setCurrent] = useState(null);
  const [labeled, setLabeled] = useState(false);

  const handleCurrent = (c) => {
    setCurrent(c);
    console.log("current", current);
  };
  useEffect(() => {
    //FIXME: Metalabels in the same sheet with the same columns are shared once added for one
    //TODO: Add fields for project related data
    console.log(results)
    if (results != undefined && results.some((e) => e.fpath === fPath)) {
      setLabeled(true);
      //console.log(results);
      results.map((val, index) => {
        return val.mlabelList.map((innerVal, innerIndex) => {
          if (innerVal.label === currentHeader) {
            console.log(innerVal.metalabel);
            handleCurrent(innerVal.metalabel);
          }
        });
      });
    }
  }, [results]);
  const selectOptions = metalabels.map((val, index) => {
    return <option key={index}>{val.metalabel}</option>;
  });

  return (
    <Form.Select
      value={current}
      id={currentHeader}
      onChange={handleSelections}
      controlId="url"
    >
      <option>null</option>
      {selectOptions}
    </Form.Select>
  );
}

export { MetaLabeSelect };
