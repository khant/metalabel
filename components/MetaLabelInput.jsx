import { clsx } from "clsx";
import Form from "react-bootstrap/Form";
import { useState, useEffect, useRef } from "react";
import axios from "axios";
import { InputGroup } from "react-bootstrap";
import { MetaLabeSelect } from "./MetaLabelSelect";
import ObjectID from "mongo-objectid";

function MetaLabelInput({ fURL, headers, handleMetalabelInput }) {
  const [unit, setUnit] = useState();
  const [metaLabels, setMetaLabels] = useState([]);
  const HeaderRef = useRef(null);

  useEffect(() => {
    handleMetalabelInput(metaLabels);
  }, [metaLabels]);

  let metaLabelTemplate = {};

  const handleInput = (e) => {};

  const onFormSubmit = (e) => {
    e.preventDefault();
  };
  const handleBlur = ({ target }) => {
    setUnit(target.value);
    metaLabels.forEach((val, index) => {
      if (val.label === target.id) {
        metaLabels[index].unit = target.value;
      }
    });
  };
  //Taken from: https://stackoverflow.com/questions/20669166/how-to-efficiently-check-if-a-key-value-pair-exists-in-a-javascript-dictionary
  function hasKeySetTo(obj, key, value) {
    return obj.hasOwnProperty(key) && obj[key] == value;
  }

  const handleSelections = (e) => {
    metaLabelTemplate = {
      file: fURL,
      label: e.target.id,
      label_id: new ObjectID().toString(),
      metalabel: e.target.value,
      metalabel_id: new ObjectID().toString(),
      unit: null,
    };
    setMetaLabels([...metaLabels, metaLabelTemplate]);
  };
  const headerItems = headers.map((header, index) => {
    if (header === "") {
      return (
        <div key={index}>
          <Form.Select
            disabled
            natype="path"
            placeholder={`Empty header at posiiton ${index}`}
          />
          <br />
        </div>
      );
    }
    return (
      <div key={index}>
        <InputGroup>
          <InputGroup.Text id={index} ref={HeaderRef}>
            {header}
          </InputGroup.Text>
          <MetaLabeSelect currentHeader={header} handleSelections={handleSelections} fPath={fURL} />
          <InputGroup.Text>Unit (optional)</InputGroup.Text>
          <Form.Control
            id={header}
            onBlur={handleBlur}
            onChange={handleInput}
            natype="sting"
            placeholder=""
          />
        </InputGroup>
        <br />
      </div>
    );
  });

  return (
    <>
      <Form onSubmit={onFormSubmit}>
        <Form.Group className="mb-3" controlId="url">
          {headerItems}
        </Form.Group>
      </Form>
    </>
  );
}

export { MetaLabelInput };
