import React from 'react';
import ComponentCreator from '@docusaurus/ComponentCreator';

export default [
  {
    path: '/metalabel/__docusaurus/debug',
    component: ComponentCreator('/metalabel/__docusaurus/debug', '007'),
    exact: true
  },
  {
    path: '/metalabel/__docusaurus/debug/config',
    component: ComponentCreator('/metalabel/__docusaurus/debug/config', '8b5'),
    exact: true
  },
  {
    path: '/metalabel/__docusaurus/debug/content',
    component: ComponentCreator('/metalabel/__docusaurus/debug/content', 'a23'),
    exact: true
  },
  {
    path: '/metalabel/__docusaurus/debug/globalData',
    component: ComponentCreator('/metalabel/__docusaurus/debug/globalData', '154'),
    exact: true
  },
  {
    path: '/metalabel/__docusaurus/debug/metadata',
    component: ComponentCreator('/metalabel/__docusaurus/debug/metadata', 'b99'),
    exact: true
  },
  {
    path: '/metalabel/__docusaurus/debug/registry',
    component: ComponentCreator('/metalabel/__docusaurus/debug/registry', 'ea2'),
    exact: true
  },
  {
    path: '/metalabel/__docusaurus/debug/routes',
    component: ComponentCreator('/metalabel/__docusaurus/debug/routes', '746'),
    exact: true
  },
  {
    path: '/metalabel/log',
    component: ComponentCreator('/metalabel/log', '448'),
    exact: true
  },
  {
    path: '/metalabel/log/archive',
    component: ComponentCreator('/metalabel/log/archive', '158'),
    exact: true
  },
  {
    path: '/metalabel/log/first-blog-post',
    component: ComponentCreator('/metalabel/log/first-blog-post', '95d'),
    exact: true
  },
  {
    path: '/metalabel/log/long-blog-post',
    component: ComponentCreator('/metalabel/log/long-blog-post', 'ad3'),
    exact: true
  },
  {
    path: '/metalabel/log/mdx-blog-post',
    component: ComponentCreator('/metalabel/log/mdx-blog-post', '397'),
    exact: true
  },
  {
    path: '/metalabel/log/tags',
    component: ComponentCreator('/metalabel/log/tags', 'b12'),
    exact: true
  },
  {
    path: '/metalabel/log/tags/docusaurus',
    component: ComponentCreator('/metalabel/log/tags/docusaurus', '3ad'),
    exact: true
  },
  {
    path: '/metalabel/log/tags/facebook',
    component: ComponentCreator('/metalabel/log/tags/facebook', '3b3'),
    exact: true
  },
  {
    path: '/metalabel/log/tags/hello',
    component: ComponentCreator('/metalabel/log/tags/hello', '87b'),
    exact: true
  },
  {
    path: '/metalabel/log/tags/hola',
    component: ComponentCreator('/metalabel/log/tags/hola', 'a00'),
    exact: true
  },
  {
    path: '/metalabel/log/welcome',
    component: ComponentCreator('/metalabel/log/welcome', '3db'),
    exact: true
  },
  {
    path: '/metalabel/markdown-page',
    component: ComponentCreator('/metalabel/markdown-page', '969'),
    exact: true
  },
  {
    path: '/metalabel/docs',
    component: ComponentCreator('/metalabel/docs', 'ba5'),
    routes: [
      {
        path: '/metalabel/docs/category/tutorial---basics',
        component: ComponentCreator('/metalabel/docs/category/tutorial---basics', '721'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/metalabel/docs/category/tutorial---extras',
        component: ComponentCreator('/metalabel/docs/category/tutorial---extras', '038'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/metalabel/docs/Get Started/intro',
        component: ComponentCreator('/metalabel/docs/Get Started/intro', '51e'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/metalabel/docs/intro',
        component: ComponentCreator('/metalabel/docs/intro', '2ae'),
        exact: true,
        sidebar: "tutorialSidebar"
      },
      {
        path: '/metalabel/docs/Metalabel Documentaton/intro',
        component: ComponentCreator('/metalabel/docs/Metalabel Documentaton/intro', '79f'),
        exact: true,
        sidebar: "tutorialSidebar"
      }
    ]
  },
  {
    path: '/metalabel/',
    component: ComponentCreator('/metalabel/', '94b'),
    exact: true
  },
  {
    path: '*',
    component: ComponentCreator('*'),
  },
];
