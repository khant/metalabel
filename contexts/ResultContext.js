// Implementation taken from: https://codesandbox.io/s/context-api-fetch-data-provider-example-0rymy?file=/src/Users.jsx
import React, { useContext, useState, useEffect, createContext } from "react";
import axios from "axios";

const ResultContext = createContext();

export function ResultsProvider({ children }) {
  const [results, setResults] = useState();
  useEffect(() => {
    async function fetchData() {
      const { data } = await axios.get("/api/results_read");
      setResults(data.dataList);
    }
    fetchData();
  }, []);
  return (
    <ResultContext.Provider
      value={{
        results,
        setResults,
      }}
    >
      {children}
    </ResultContext.Provider>
  );
}

export function useResult() {
  const context = useContext(ResultContext);
  if (context === undefined) {
    throw new Error("Context must be used within a Provider");
  }
  return context;
}
