// Implementation taken from: https://codesandbox.io/s/context-api-fetch-data-provider-example-0rymy?file=/src/Users.jsx
import React, { useContext, useState, useEffect, createContext } from "react";
import axios from "axios";

const MetalabelContext = createContext();

export function MetalabelProvider({ children }) {
  const [metalabels, setMetalabels] = useState();
  useEffect(() => {
    async function fetchData() {
      const { data } = await axios.get("/api/metalabels_read");
      setMetalabels(data);
    }
    fetchData();
  }, []);
  return (
    <MetalabelContext.Provider
      value={{
        metalabels,
        setMetalabels,
      }}
    >
      {children}
    </MetalabelContext.Provider>
  );
}

export function useMetalabels() {
  const context = useContext(MetalabelContext);
  if (context === undefined) {
    throw new Error("Context must be used within a Provider");
  }
  return context;
}
