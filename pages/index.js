import Head from "next/head";
import styles from "../styles/Home.module.css";
import { Alert } from "react-bootstrap";

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Metalabel</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>Metalabel 📚</h1>

        <p className={styles.description}>
          Semantic labelling for tabular data in CSV and Excel files.
        </p>

        {/*  <Alert className={styles.pitch} variant="primary">

       Labeled data is a group of samples that have been tagged with one or more labels. Labeling typically takes a set of unlabelled data and augments each piece of it with informative tags. For example, a data label might indicate what type of scientific variable is described by the data in a column, is it a physical or a chemical or a biological variable, what variable group does data in a column belong to and so on.

Building heterogeneous datasets from CSV Excel and JSON files is exhaustive, as the count of unknown labels in your dataset grows. This makes it challenging for your data analysis to keep track of the vocabulary used in column headers. Especially as your datasets grow from just a few files to hundreds or thousands. With Metalabel (name of software), you can tag and group your labels for smoother querying and data analysis.
          Labeled data is a group of samples that have been tagged with one or
          more labels. Labeling typically takes a set of unlabeled data and
          augments each piece of it with informative tags. For example, a data
          label might indicate whether a photo contains a horse or a cow, which
          words were uttered in an audio recording, what type of action is being
          performed in a video, what the topic of a news article is, what the
          overall sentiment of a tweet is, or whether a dot in an X-ray is a
          tumor.
          <br/>
          <br/>
          Building hetergenous datasets from CSV Excel and JSON files is
          tiring, as the count of unknown labels in your dataset grows. This
          makes it challenging for your data analysis to keep track of the
          vocaulary used in column headers and key-values. Especially as your
          datasets grows from just a few files to hundreds or thousands. With
          Metalabel📚, you can tag and group your labels for smoother querying
          and data anylsis.
        </Alert> */}
        <div className={styles.grid}>
          <a target="_blank" href="https://khant.pages.ufz.de/metalabel/" className={styles.card}>
            <h3>Documentation &rarr;</h3>
            <p>Find in-depth information about Metalabel features.</p>
          </a>
          <a href="/add" className={styles.card}>
            <h3>Tool &rarr;</h3>
            <p>Get started with labeling your datasets!</p>
          </a>
        </div>
      </main>

      <footer className={styles.footer}>
        <a href="https://www.ufz.de/index.php?en=49404">
          Copyright 2022. Taimur Khan.
          {/*  <img src="/ufz.png" alt="UFZ Logo" className={styles.logo} /> */}
        </a>
      </footer>
    </div>
  );
}
