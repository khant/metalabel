// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import fs from "fs";
import path from "path";

function walkDir(dir, callback) {
  fs.readdirSync(dir).forEach((f) => {
    let dirPath = path.join(dir, f);
    let isDirectory = fs.statSync(dirPath).isDirectory();
    isDirectory ? walkDir(dirPath, callback) : callback(path.join(dir, f));
  });
}

export default function fswalkHandler(req, res) {
  console.log(req.body);
  let fpath = req.body.url;

  let trimmed_fpath = fpath.trim();
  const EXTENSION_CSV = ".csv";
  const EXTERNSION_XLSX = ".xlsx";
  const EXTENSION_XLS = ".xls";
  var files = [];
  //console.log(trimmed_fpath);
  if (req.body.csv === true && req.body.xlsx === false) {
    walkDir(trimmed_fpath, function (path) {
      if (path.endsWith(EXTENSION_CSV)) {
        const stats = fs.statSync(path);
        const csvOnlyObj = { type: "csv", path: path, stats: stats };
        files.push(csvOnlyObj);
      }
    });
  }

  if (req.body.csv === false && req.body.xlsx === true) {
    walkDir(trimmed_fpath, function (path) {
      if (path.endsWith(EXTERNSION_XLSX) || path.endsWith(EXTENSION_XLS)) {
        const stats = fs.statSync(path);
        const xlsxOnlyObj = { type: "xlsx", path: path, stats: stats };
        files.push(xlsxOnlyObj);
      }
    });
  }

  if (req.body.csv === true && req.body.xlsx === true) {
    walkDir(trimmed_fpath, function (path) {
      if (path.endsWith(EXTENSION_CSV)) {
        const stats = fs.statSync(path);
        const csvObj = { type: "csv", path: path, stats: stats };
        files.push(csvObj);
      }
      if (path.endsWith(EXTERNSION_XLSX) || path.endsWith(EXTENSION_XLS)) {
        const stats = fs.statSync(path);
        const xlsxObj = { type: "xlsx", path: path, stats: stats };
        files.push(xlsxObj);
      }
    });
  }
  /*  walkDir(trimmed_fpath, function(filePath) {
    //const fileContents = fs.readFileSync(filePath, 'utf8');
    //const splitPath = filePath.split(fpath)
    //console.log(splitPath[1]);
    if(filePath.includes(EXTENSION)) files.push(filePath);
    //res.send(files);
  }); */
  //console.log(files);
  res.status(200).json({ data: files });
  files = [];
}
