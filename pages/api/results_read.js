import fs from "fs";
import { env } from "process";

export const config = {
  api: {
    responseLimit: false,
  },
};

export default async function fileHandler(req, res) {
  let doc = JSON.parse(
    fs.readFileSync(`./outputs/results/${env.METALABEL_PROJECT_NAME}.json`)
  );
  res.status(200).json(doc);
}
