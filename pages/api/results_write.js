import { JsonDB, Config } from "node-json-db";
import fs from "fs";
import path from "path";
import { env } from "process";
import md5File from "md5-file";

// The first argument is the database filename. If no extension, '.json' is assumed and automatically added.
// The second argument is used to tell the DB to save after each push
// If you put false, you'll have to call the save() method.
// The third argument is to ask JsonDB to save the database in an human readable format. (default false)
// The last argument is the separator. By default it's slash (/)
var db = new JsonDB(
  new Config(
    `./outputs/results/${env.METALABEL_PROJECT_NAME}.json`,
    true,
    true,
    "/"
  )
);

export default async function fileHandler(req, res) {
  function get_url_extension(url) {
    return url.split(/[#?]/)[0].split(".").pop().trim();
  }
  let data = req.body.metalabel;

  //Data is refactored to fit the ML data model
  let refactoredData = {
    fpath: data[0].file,
    md5: md5File.sync(data[0].file),
    ftype: get_url_extension(data[0].file),
    mlabelList: data.map(({ file, ...keepAttrs }) => keepAttrs),
  };
  const existingFile = await db.getData("/dataList/");
  //TODO: TK dataList array not created automatically
  await db.push("/dataList[]/", refactoredData);
  res.status(200);
  res.send(existingFile);
}
