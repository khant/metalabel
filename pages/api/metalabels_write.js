import { JsonDB, Config } from "node-json-db";
import { env } from "process";
import fs from "fs";
import path from "path";

// The first argument is the database filename. If no extension, '.json' is assumed and automatically added.
// The second argument is used to tell the DB to save after each push
// If you put false, you'll have to call the save() method.
// The third argument is to ask JsonDB to save the database in an human readable format. (default false)
// The last argument is the separator. By default it's slash (/)
const replacementName = "metalabels";
var db = new JsonDB(
  new Config(
    `./outputs//metalabels/${
      env.METALABEL_PROJECT_NAME || replacementName
    }.json`,
    true,
    true,
    "/"
  )
);

export default async function fileHandler(req, res) {
  let data = req.body.doc;
  /*   const current = await db.getData("/").catch((err) => {
    db.push("/", []);
  });
  console.log(current); */
  await db.push("/", data).catch((err) => {
    console.log("DB Error", err);
  });
  res.send(200);
}
