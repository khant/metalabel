// Next.js Dynamic API route support: https://nextjs.org/docs/api-routes/dynamic-api-routes
import fs from "fs";
import { readExcel, toJSON } from "danfojs-node";
const dfd = require("danfojs-node");
const path = require("path");
var XLSX = require('xlsx');


export const config = {
  api: {
    responseLimit: false,
  },
};
export default async function fileHandler(req, res) {
  let file = req.body.file;
  let ftype = req.body.ftype;
  //Adapted from StackOverflow: https://stackoverflow.com/questions/63066985/send-file-as-response-using-nextjs-api
  var stat = fs.statSync(file);
  if (ftype === "csv") {
    res.writeHead(200, {
      "Content-Type": "text/csv",
      "Content-Length": stat.size,
    });
    var readStream = fs.createReadStream(file);
    // We replaced all the event handlers with a simple call to readStream.pipe()
    readStream.pipe(res);
  }
  if (ftype === "xlsx") {
    var dataArray = [];
    var xlsxData = XLSX.readFile(file); // parses a file
    var worksheetsArray = xlsxData.SheetNames; // gets the name of first sheet
    worksheetsArray.forEach( (sheetName) => {
      let sheetObject = {};
      let json = XLSX.utils.sheet_to_json(xlsxData.Sheets[sheetName]);
      sheetObject.name = sheetName;
      sheetObject.data = json;
      dataArray.push(sheetObject);
    });
    res.status(200).json(dataArray);
    dataArray = []; 
  }
}
