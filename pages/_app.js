import "../styles/globals.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { ResultsProvider } from "../contexts/ResultContext";
import { MetalabelProvider } from "../contexts/MetalabelContext";
import { useContext, useEffect, useState } from "react";
import "../styles/bootstrap/App.scss";
import axios from "axios";

function MyApp({ Component, pageProps }) {
  /* const [results, setResults] = useState();
  const [metalabels, setMetalabels] = useState();
   */
/*   //Call metalabels 
  useEffect(() => {
    axios
      .get("/api/metalabels_read")
      .then((res) => {
        setMetalabels(res.data);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        console.log(metalabels);
      });
  }, []);
 */
 /*  //Call results 
  useEffect(() => {
    const handleResults = () => {
      axios
        .get("/api/results_read")
        .then((res) => {
          setResults(res.data);
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => {
          console.log(results);
          
        });
    };
    handleResults();
  }, []); */

  return (
    <ResultsProvider>
      <MetalabelProvider>
        <Component {...pageProps} />
      </MetalabelProvider>
    </ResultsProvider>
  );
}

export default MyApp;
