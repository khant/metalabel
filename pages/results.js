import { useEffect, useState, useContext } from "react";
import Head from "next/head";
import styles from "../styles/Results.module.css";
import { NavSimple } from "../components/NavSimple";
import axios from "axios";
import { Form, Button, Container, InputGroup, Alert } from "react-bootstrap";
import dynamic from "next/dynamic";
import { useMetalabels } from "../contexts/MetalabelContext";
import { useResult } from "../contexts/ResultContext";
const DynamicReactJson = dynamic(import("react-json-view"), { ssr: false });

function Results() {
  const { metalabels, setMetalabels } = useMetalabels();
  const { results, setResults } = useResult();

  const handleEdit = (e) => {
    console.log(e);
    dataUpdateCallback(e.updated_src);
  };

  const handleAdd = (e) => {
    console.log(e);
    dataUpdateCallback(e.updated_src);
  };

  const handleDelete = (e) => {
    console.log(e);
    dataUpdateCallback(e.updated_src);
  };
  const exportData = () => {
    const jsonString = `data:text/json;chatset=utf-8,${encodeURIComponent(
      JSON.stringify(results)
    )}`;
    const link = document.createElement("a");
    link.href = jsonString;
    link.download = `results_${process.env.NEXT_PUBLIC_METALABEL_PROJECT_NAME}.json`;

    link.click();
  };

  return (
    <>
      <Container className={styles.main}>
        <Head>
          <title>Metalabel</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <NavSimple />
        <Button style={{  marginTop: "5%" }} onClick={exportData}>💾 Download JSON</Button>
        <DynamicReactJson
          name="metalabels"
          onDelete={false}
          onEdit={false}
          onAdd={false}
          theme="normal"
          src={results}
          style={{ width: "100%", marginTop: "5%" }}
        />
      </Container>
    </>
  );
}
export default Results;
