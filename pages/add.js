import { useEffect, useState, useContext } from "react";
import Head from "next/head";
import { useSemiPersistantState } from "../hooks/SemiPersistantState";
import styles from "../styles/Create.module.css";
import { NavSimple } from "../components/NavSimple";
import axios from "axios";
import { Form, Button, Container, InputGroup, Alert } from "react-bootstrap";
import clsx from "clsx";
import dynamic from "next/dynamic";
import ObjectID from "mongo-objectid";
import { useMetalabels } from "../contexts/MetalabelContext";
import { SaveToast } from "../components/SaveToast";
const DynamicReactJson = dynamic(import("react-json-view"), { ssr: false });

function CreateMetalabels() {
  const { metalabels, setMetalabels } = useMetalabels();
  const [n, setN] = useState(false);
  const [showAlert, setShowAlert] = useState(false);
  const [inputMetalabel, setMetalabelInput] = useState();
  const [description, setDescription] = useState();
  const [showToast, setShowToast] = useState(false);

  const handleToast = (val) => {
    setShowToast(val);
  };
  const handleEdit = (e) => {
    console.log(e);
    dataUpdateCallback(e.updated_src);
  };

  const handleAdd = (e) => {
    console.log(e);
    dataUpdateCallback(e.updated_src);
  };

  const handleDelete = (e) => {
    console.log(e);
    dataUpdateCallback(e.updated_src);
  };

  const handleSave = () => {
    axios
      .post("http://localhost:3000/api/metalabels_write", { doc: metalabels })
      .then((res) => {
        if (res.status == 200) {
          setShowToast(true);
        }
      })
      .catch((err) => {
        console.log(err);
      });
    /*       .finally(() => {
      }); */
  };

  const handleAddbutton = (e) => {
    let doc2 = metalabels || [];
    let m = {
      metalabel: inputMetalabel,
      description: description,
      _id: new ObjectID().toString(),
    };
    doc2.push(m);
    dataUpdateCallback(doc2);
    setShowToast(true);
    console.log("add button function");
  };
  const dataUpdateCallback = (data) => {
    setMetalabels(data);
  };

  useEffect(() => {
    setN(true);
    setShowAlert(true);
  }, []);

  useEffect(() => {
    console.log("metalabels", metalabels);
    //handleSave(metalabels);
  }, [metalabels]);
  return (
    <>
      <Container className={styles.main}>
        <Head>
          <title>Metalabel</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <NavSimple />
        <InputGroup className="mb-3">
          <Form.Control
            onChange={(i) => setMetalabelInput(i.target.value)}
            name="metalabel"
            placeholder="Metalabel"
          />
          <Form.Control
            onChange={(i) => setDescription(i.target.value)}
            name="descrition"
            placeholder="Description"
          />
          <Button
            onClick={handleAddbutton}
            variant="outline-primary"
            id="button-addon2"
          >
            Add
          </Button>
          <Button onClick={handleSave} variant="primary" id="button-addon2">
            💾
          </Button>
        </InputGroup>
        {n ? (
          <DynamicReactJson
            name="metalabels"
            onDelete={handleDelete}
            onEdit={handleEdit}
            onAdd={false}
            theme="monokai"
            src={metalabels}
            style={{ width: "100%" }}
          />
        ) : (
          <Alert className={styles.pitch} variant="primary">
            <Alert.Heading>No metalbels for this project yet ⛔️</Alert.Heading>
            <hr />
            <p>
              Start adding metalabels to your project. Metalabels are the
              building blocks of your project. Metalabels are the labels that
              you will use to organize your data. All metalabels are saved to an
              JSON file that is stored in the outputs folder. You can edit the
              JSON file directly or use the interface to add metalabels.
            </p>
          </Alert>
        )}
      </Container>
      <footer className={clsx({ [styles.footer]: !n, [styles.footer2]: n })}>
        <a>
          Taimur Khan. © 2022.
          {/* <img src="/ufz.png" alt="UFZ Logo" className={styles.logo} /> */}
        </a>
      </footer>
      <SaveToast show={showToast} setShow={handleToast} />
    </>
  );
}

export default CreateMetalabels;
