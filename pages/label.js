import { useEffect, useState } from "react";
import Head from "next/head";
import styles from "../styles/Tool.module.css";
import { InputBar } from "../components/InputBar";
import { FileCard } from "../components/FileCard";
import axios from "axios";
import { Alert, Container } from "react-bootstrap";
import clsx from "clsx";

var paths;
function Tool() {
  const [pathsAvailable, setPathsAvailable] = useState(false);
  const [pathContent, setPathContent] = useState([]);
  const [buildCards, setBuildCards] = useState(false);

  const handleData = (callback) => {
    paths = callback.data;
    setPathContent(callback.data);
    setPathsAvailable(true);
  };
  useEffect(() => {
    if (pathsAvailable) {
      setBuildCards(true);
    }
  }, [pathsAvailable]);
  return (
    <>
      <Container>
        <Head>
          <title>Metalabel</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <InputBar dataCallback={handleData} />

        {buildCards ? (
          <FileCard files={pathContent} />
        ) : (
          <Alert className={styles.pitch} variant="primary">
            <Alert.Heading>Get started ➡</Alert.Heading>
            <hr />
            <p>
              Please enter the URL for the folder with your excel or csv files in
              the input box above. At least one file type (excel or csv) must be selected before searching. 
            </p>
          </Alert>
        )}
      </Container>
      <footer className={clsx({[styles.footer]: !buildCards}, {[styles.footerCard] : buildCards})}>
        <a>
          Taimur Khan. © 2022.
          {/* <img src="/ufz.png" alt="UFZ Logo" className={styles.logo} /> */}
        </a>
      </footer>
    </>
  );
}

export default Tool;
