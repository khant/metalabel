import { useEffect, useState } from "react";

const useSemiPersistantState = (key) => {
  const [value, setValue] = useState(localStorage.getItem(key) || "");

  useEffect(() => {
    localStorage.setItem(key, value);
  }, [value, key]);

  return [value, setValue];
};
export default useSemiPersistantState;
